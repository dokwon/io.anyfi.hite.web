// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require fancybox
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require_tree .

function mail_clicked() {
  window.location ='mailto:team@anyfi.io';
  return false;
}

function facebook_clicked() {
  location.href = "https://www.facebook.com/anyfiapp";
}

function twitter_clicked() {
  location.href = "https://twitter.com/anyfi_network";
}

function android_download() {
  location.href = "https://play.google.com/store/apps/details?id=io.anyfi.absolut.zero";
}

function windows_download() {
  // TODO: not implemented
}

function scroll_top() {
	$(document).scrollTop(0);
}



var animations = [
  [
    ['#banner_1', 'fadeIn'],
    ['#banner_6', 'fadeIn']
  ], 
  [
    ['#banner_1', 'fadeOut']
  ], 
  [
    ['#banner_2', 'fadeIn'],
    ['#banner_3', 'fadeIn'],
    ['#banner_4', 'fadeIn'],
    ['#banner_5', 'fadeIn']
  ],
  [
    ['#banner_3', 'fadeOut'],
    ['#banner_5', 'fadeOut']
  ],
  [
    ['#banner_2', 'fadeOut'],
    ['#banner_4', 'fadeOut'],
    ['#banner_6', 'fadeOut']
  ]
];

function hide(element) {
  element.css('opacity', 0);
  element.animate({
            'height': 0,
            'opacity': 0
        }, 750, function() {
            $(this).hide();
        });
}

var index = 0;

function animate_banner() {
  var cycle = animations[index % animations.length];

  var done_task_counter = 0; 

  for(var i =0; i < cycle.length; i ++) {
    var element_id = cycle[i][0];
    var animation = cycle[i][1];
    
    if (animation == 'fadeIn') {
      $(element_id).show();
    }

    if($(element_id).length) {
      $(element_id).addClass(animation + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        $(this).removeClass(animation + ' animated');

        if (animation == 'fadeOut') {
          $(this).hide();
        }
        done_task_counter += 1; 
        if (done_task_counter >= cycle.length) {
          index += 1; 
          animate_banner();
        }
      })
    }
  }
}

$(document).ready(function() {
  $(".fancybox").fancybox();
  $('.fancybox-media')
        .attr('rel', 'media-gallery')
        .fancybox({
          openEffect : 'none',
          closeEffect : 'none',
          prevEffect : 'none',
          nextEffect : 'none',
          width     : 640,
          height    : 385,

          arrows : false,
          helpers : {
            media : {},
            buttons : {}
          }
        });

  $(".dropdown-toggle").click(function(e){
    console.log("ddd");
      return false;
  });
  
});