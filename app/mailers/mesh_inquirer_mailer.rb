class MeshInquirerMailer < ApplicationMailer
  default from: 'from@example.com'
  layout 'mailer'

  def mesh_inquire_email
    mail(to: "team@anyfi.io", subject: 'Anyfi Mesh SDK Inquiry')
  end
end
