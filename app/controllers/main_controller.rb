class MainController < ApplicationController

  @locale_checked = false
  

  def entry
    @page_index = 0
    @navbtn_class = 'nav-btn-lg'
    @product_tab_class ='product_tab'
    @tab_class = 'nav-item'

    # only check locale once
    if @locale_checked == false then
      locale = Timeout::timeout(5) { Net::HTTP.get_response(URI.parse('http://api.hostip.info/country.php?ip=' + request.remote_ip )).body } rescue "KO"
      puts locale
      if locale == "KO" then
        I18n.default_locale = :ko
      else
        I18n.default_locale = :en 
      end
      @locale_checked = true
    end
  end

  def enterprise
  	@page_index = 2
    @navbtn_class = 'nav-btn-sm'
    @product_tab_class = 'b2b_product_tab' 
    @tab_class = 'b2b-nav-item'
  end

  def agreement
    @page_index = 0
    @navbtn_class = 'nav-btn-sm'
    @product_tab_class ='product_tab'
    @tab_class = 'nav-item'
  end

  def default_url_options(options={})
    { locale: I18n.locale }
  end

end
