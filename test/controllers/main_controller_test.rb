require 'test_helper'

class MainControllerTest < ActionDispatch::IntegrationTest
  test "should get entry" do
    get main_entry_url
    assert_response :success
  end

  test "should get consumertransition" do
    get main_consumertransition_url
    assert_response :success
  end

  test "should get enterprisetransition" do
    get main_enterprisetransition_url
    assert_response :success
  end

end
