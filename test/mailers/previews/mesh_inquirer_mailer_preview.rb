# Preview all emails at http://localhost:3000/rails/mailers/mesh_inquirer_mailer
class MeshInquirerMailerPreview < ActionMailer::Preview
  def mesh_email_preview
    MeshInquirer.mesh_inquire_email
  end
end
