Rails.application.routes.draw do
  
	scope ":locale", locale: /en|ko/ do

		get "main/entry"
		get "main/consumer"
		get "main/enterprise"
		get "main/agreement"
	  
	end
	get '*path', to: redirect("/#{I18n.default_locale}/%{path}"), constraints: lambda { |req| !req.path.starts_with? "/#{I18n.default_locale}/" }
	get '', to: redirect("/#{I18n.default_locale}/main/entry")
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'main#entry'
  

end
